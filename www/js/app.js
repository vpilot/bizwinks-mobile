var app = angular.module('bizwinks', [
	'ionic',
	'ngCordova',
	'ngMap',
	'bizwinks.controllers',
	'bizwinks.services',
	'bizwinks.filters',
	'bizwinks.router',
	'bizwinks.directives'
]);

app.run(function ($ionicPlatform) {
	$ionicPlatform.ready(function () {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}

		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
});

// TODO - introduce Tokens for WB public APIs
// app.config(function ($httpProvider) {
// 	$httpProvider.defaults.headers.common['Authorization'] = 'Token 74dc6040091d46bd0b3bce95f94eeea9a2af07e8';
//});

app.config(function ($sceDelegateProvider) {
	$sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'https://admin.bizwinks.com/**',
		'https://streams.winkball.com/**',
		// Allow google apis
		'http://maps.googleapis.com/**',
		'https://maps.googleapis.com/**',
		// 'mailto:**'
  ]);
});
