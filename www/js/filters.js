var app = angular.module('bizwinks.filters', []);

app.filter("join", function () {
	return function (arr, sep) {
		return arr.join(sep);
	};
});

app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

app.filter("address", function () {
	return function (biz) {
		var address = biz.businessProfile.postalAddress;
		var display_address = '';

		if(address.firstLineOfAddress) {
			display_address += address.firstLineOfAddress + ', ';
		}

		if(address.secondLineOfAddress) {
			display_address += address.secondLineOfAddress + ', ';
		}

		if(address.postcode) {
			display_address += address.postcode;
		}

		return display_address;
	};
});

app.filter("isEmpty", function () {
	return function (obj) {
		if(!obj) {
			return true;
		}
		return Object.keys(obj).length === 0;
	};
});

app.filter("not", function () {
	return function (obj) {
		return !obj;
	};
});
