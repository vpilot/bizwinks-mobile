var app = angular.module('bizwinks.services', []);

app.service("ViewableService", function ($q, $http, $cordovaGeolocation, $ionicPopup) {
	var self = {
		'page': 1,
		'isLoading': false,
		'hasMore': true,
		'results': [],
		'lat': 51.479863,
		'lon': -0.1989835,
		'length': 20,
		'offset' : 0,
		'searchTerm': '',

		'refresh': function (searchTerm) {
			self.page = 1;
			self.offset = 0;
			self.isLoading = false;
			self.hasMore = true;
			self.results = [];
			if(searchTerm) {
				self.searchTerm = searchTerm;
			} else {
				self.searchTerm = '';
			}
			return self.load();
		},

		'next': function (searchTerm) {
			self.page += 1;
			self.offset += 20;
			if(searchTerm) {
				self.searchTerm = searchTerm;
			}
			return self.load();
		},

		'load': function (searchTerm) {
			self.isLoading = true;
			if(searchTerm) {
				self.searchTerm = searchTerm;
			}
			var deferred = $q.defer();

				ionic.Platform.ready(function(){
					$cordovaGeolocation.getCurrentPosition({timeout:10000, enableHighAccuracy: true})
					.then(function(position){

						self.lat = position.coords.latitude;
						self.lon = position.coords.longitude;

						var params = {
//							page: self.page,
							length: self.length,
							offset: self.offset,
							order: 'CREATED_DATE',
							direction: 'DESCENDING',
//							lat: self.lat,
//							lon: self.lon
						};

						if(self.searchTerm) {
							params.q = self.searchTerm;
						}

						console.log(params);

						$http.get('https://admin.bizwinks.com/public/businesses/search', {params: params})
							.success(function (data) {
								self.isLoading = false;

								if (data.total === 0 || Object.keys(data.businessProfiles).length === 0) {
									self.hasMore = false;
								} else {
									if(Array.isArray(data.businessProfiles.businessProfileExtended)) {
										angular.forEach(data.businessProfiles.businessProfileExtended, function (business) {
											self.results.push(business);
										});
									} else {
										// result contains single item
										self.results.push(data.businessProfiles.businessProfileExtended);
									}
								}

								deferred.resolve();
							})
							.error(function (data, status, headers, config) {
								self.isLoading = false;
								deferred.reject(data);
							});
						}, function(err){

							//console.error('Error getting position.');
							//console.error(err);

							$ionicPopup.alert({
								'title' : 'Please switch on geolocation',
								'template': "It seems like you've switched off geolocation, please switch it on by going to your application settings."
							});
						});
					});

			return deferred.promise;
		},

		'getResult' : function(index) {
			return self.results[index];
		}
	};

	// self.load();

	return self;
});
