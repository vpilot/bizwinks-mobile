var app = angular.module('bizwinks.controllers', []);

app.controller("ViewableController", function ($scope, $state, $ionicLoading, ViewableService) {
	$scope.viewable = ViewableService;

	$ionicLoading.show({template:'Loading...'});
	$scope.viewable.load().then(function(){
		$ionicLoading.hide();
	});

	$scope.doRefresh = function(searchTerm) {
		if (!$scope.viewable.isLoading) {
			$ionicLoading.show({template:'Loading...'});
			$scope.viewable.refresh(searchTerm).then(function(){
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			});
		}
	};

	$scope.loadMore = function(searchTerm) {
		if (!$scope.viewable.isLoading && $scope.viewable.hasMore) {
			$ionicLoading.show({template:'Loading...'});
			$scope.viewable.next(searchTerm).then(function(){
				$ionicLoading.hide();
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
	};

	$scope.getDirection = function(biz) {

		var destination = [
			biz.businessProfile.location.latitude,
			biz.businessProfile.location.longitude
		];

		var source = [
			$scope.viewable.lat,
			$scope.viewable.lon
		];

		launchnavigator.navigate(destination, source);
	};

	$scope.openBusinessDetail = function(index) {
		$state.go('business', { business: parseInt(index)});
	};

	$scope.searchBiz = function() {

		var searchStr = $scope.search.txt;
		$scope.search.show = !$scope.search.show;

		// toggle opening and searching
		if($scope.search.show) {
			return;
		}

		if(searchStr.length >= 3) {
			$scope.doRefresh($scope.search.txt);
		} else {
			$scope.doRefresh();
		}
	};

});

app.controller('BusinessController', function($scope, $window, $cordovaSocialSharing, business, ViewableService, NgMap) {
	// console.log(business);
  $scope.business = business;
	$scope.viewable = ViewableService;

	if(business.businessProfile.links && Object.keys(business.businessProfile.links).length !== 0) {
		// if there is only one link WB API returnes it as an object and not an array as expected
		if(!Array.isArray(business.businessProfile.links.link)) {
			$scope.business.businessProfile.links.link = [business.businessProfile.links.link];
		}
	}

	// telephone number field is a string, sometimes results contains an erroneous data
	// and comma separated multiple numbers
	if(business.businessProfile.telephone) {
		var tels = business.businessProfile.telephone.replace(/\s/g, '').replace(/\-/g, '').split(',');
		$scope.business.businessProfile.telephone = tels[0];
	}

	NgMap.getMap().then(function(map){
		$scope.map = map;
	});

	$scope.getVideoSrc = function() {
		var id = $scope.business.businessProfileEntry.id;
		return "https://streams.winkball.com/getVideo?winkPuid="+id+"&amp;quality=web_widescreen";
	};

	$scope.getVideoPoster = function() {
		// console.log('in getVideoPoster...');
		var id = $scope.business.businessProfileEntry.id;
		return "https://streams.winkball.com/getWinkThumbnail?id="+id+"&amp;w=320&amp;h=180";
	};

	$scope.getDirection = function() {
		var destination = [
			$scope.business.businessProfile.location.latitude,
			$scope.business.businessProfile.location.longitude
		];

		var source = [
			$scope.viewable.lat,
			$scope.viewable.lon
		];

		launchnavigator.navigate(destination, source);
	};

	$scope.shareBiz = function() {
		var title = $scope.business.businessProfile.title;
		var link = "https://www.bizwinks.com/php/entry.php?id="+$scope.business.businessProfile.id;
		var message = title + ": Please check our business at BizWinks.com";
		var thumbnail = $scope.getVideoPoster();

		// console.log(title);
		// console.log(link);
		// console.log(message);
		// console.log(thumbnail);

		// Share via native share sheet
		$cordovaSocialSharing.share(message, title,	thumbnail, link);
	};

	$scope.showOnMap = function() {
		// console.log('showOnMap called...');
		var point = [
			$scope.business.businessProfile.location.latitude,
			$scope.business.businessProfile.location.longitude
		];

		var mapLink;

		if (ionic.Platform.isIOS()) {
			// http://maps.apple.com/?ll=50.894967,4.341626
			mapLink = 'http://maps.apple.com/?ll='+point[0]+','+point[1]+'&q='+$window.encodeURIComponent($scope.business.businessProfile.title);
			$window.open(mapLink, '_system', 'location=yes');
		} else {
			if(ionic.Platform.isAndroid()) {
				mapLink = 'https://maps.google.com/?q='+point[0]+','+point[1]+'('+$window.encodeURIComponent($scope.business.businessProfile.title)+')';
			} else {
				mapLink = 'geo:'+point[0]+','+point[1]+'?q='+point[0]+','+point[1]+'('+$window.encodeURIComponent($scope.business.businessProfile.title)+')';
			}
			$window.open(mapLink, '_blanc', 'location=yes');
		}

		return false;
	};

	$scope.mailTo = function() {
		var email = $scope.business.businessProfile.businessEmail;

		$window.location.href = "mailto:"+email;
	};

	$scope.call = function() {
		var telephone = $scope.business.businessProfile.telephone;

		$window.location.href = "tel:"+telephone;
	};

	$scope.getLinkIconClass = function(title) {
		if(title) {
			var t = title.toLowerCase();
			if(t === 'web') {
				return "ion-ios-world-outline";
			}

			if(t === 'facebook') {
				return "ion-social-facebook-outline";
			}

			if(t === 'twitter') {
				return "ion-social-twitter-outline";
			}
		}

		// default one
		return "ion-ios-world-outline";
	};

	$scope.openExternalUrl = function(link) {
		$window.open(link, '_system', 'location=yes');
		return false;
	};

});
