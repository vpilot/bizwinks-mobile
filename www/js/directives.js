var app = angular.module('bizwinks.directives', []);

app.directive('searchBar', function(){
	return {
		restrict: 'E',
		replace: true,
		template: '<div class="buttons" side="right">'+
			'<div class="searchBar">'+
			'<div class="searchTxt" ng-show="search.show">'+
			'<div class="bgdiv"></div>'+
			'<div class="bgtxt">'+
			'<input type="text" placeholder="Search..." ng-model="search.txt">'+
			'</div>'+
			'</div>'+
			'<i class="icon placeholder-icon" ng-click="searchBiz()"></i>'+
			'</div>'+
			'</div>',
		compile: function (element, attrs) {
			var icon = attrs.icon	||
			(ionic.Platform.isAndroid() && 'ion-android-search')	||
			(ionic.Platform.isIOS() && 'ion-ios-search')	|| 'ion-search';

			angular.element(element[0].querySelector('.icon')).addClass(icon);

			return function($scope, $element, $attrs, ctrls) {
				// console.log(ctrls);
				// var inputElement = $element.find('input')[0];
			};
		},
		controller: ['$scope', function($scope) {
			$scope.search = { txt:'', show: false};
		}]
	};
});
