var app = angular.module('bizwinks.router', []);

app.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider.state('businesses', {
    url: '/businesses',
    templateUrl: 'templates/home.html',
    controller: 'ViewableController'
  }).state('business', {
    url: '/businesses/:business',
    templateUrl: 'templates/business.html',
    controller: 'BusinessController',
    resolve: {
      business: function($stateParams, ViewableService) {
        // console.log('Resolving business....');
        // console.log($stateParams.business);
        // console.log(ViewableService);
        // console.log(ViewableService.getResult($stateParams.business));
        return ViewableService.getResult($stateParams.business);
      }
    }
  });

  $urlRouterProvider.otherwise('/businesses');

});
